# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "16.4.1"
  hashes = [
    "h1:IWEejM1ZTqLMjtsoVmzUE7jDv8VccKTwxuh46L5YSuQ=",
    "zh:291cd9a189408f51ddcc15818cc220c08b9685c307f7bffc2813c25640867d0b",
    "zh:3a1f31ae9743931cdefe88bcb9ccd69497f977c4f4e7f0cef2109c136cd2b8f3",
    "zh:43a1c27a2960ca3c9f84bf9ca11d4855093bc45578b5a7f1cc4317cf4ba7fa8e",
    "zh:545dcc8cad423b5c571fedae5395c83723a14f89e1b81a43d229ed70839f50d6",
    "zh:71c87c189e94d0a87aebc2af6b98d8b8091888dd335da6a5162e3913f922e974",
    "zh:883d5ccda2bec118dc7ad2057b98d264ed1fc86f010b5e602ab738f6fe022a46",
    "zh:9119c743178a6218aaf0d26cea5975ea758d73eaf3ffbbb03ff28d6cfabbcdcc",
    "zh:9c5b6ce04a53201380acc5c6aa5face53606fce8af71de774e2e578312ff4a64",
    "zh:a3c92e4ed975649d17679e58f66386ae4e0e488f893412ab571a02d51676560b",
    "zh:a79e4a70697f0247e98b3d05dcf5182fd2b09b4bd14cc603350db3ee8bd3e660",
    "zh:b6ca570ef929ead83e77eb15c5a8d199320ba5708a2b8e73ae58cdd99f3f9a78",
    "zh:c42d44dd8f69faaf6ce7136703836aa3c739edade93900d01e8e70bde0a93fbe",
    "zh:c780637973f7ae0a44e651ce08cb20bca062c2b030f30647fb6c3fbe42f96cc9",
    "zh:f0eeac9adf1c71708372bf1a217ea5f6a7677937b316a3784483a6022a5dbbaa",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}
