terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
    }
  }
}

variable "gitlab_token" {
  description = "Gitlab access token"
}

# Configure the GitLab Provider
provider "gitlab" {
  token = var.gitlab_token
}

# # Add a group
# resource "gitlab_group" "example_group" {
#   name        = "group-1"
#   path        = "takax"
#   description = "An example group 1"
# }

# Add a project owned by the user
resource "gitlab_project" "sample_project" {
  name = "example"
}
